from fastapi import FastAPI
from random import randrange

_QUOTES_FILE = './backend/quotes.csv'

app = FastAPI()


def read_quotes(filename: str) -> list:
    """"
    Read quotes from the given file, assuming a ; separated csv containing author-quote pairs

    Returns a list of (author, quote) tuples.
    """
    quotes = []
    with open(filename) as input_file:
        for line in input_file:
            author, quote = line.strip().split(';')
            quotes.append((author, quote))
    return quotes


def get_random_quote() -> tuple:
    quote_list = read_quotes(_QUOTES_FILE)
    random_id = randrange(len(quote_list))
    return quote_list[random_id]


@app.get('/')
async def root():
    author, quote = get_random_quote()
    return {'author': author, 'quote': quote}