from setuptools import setup

setup(
    name='Tegeltjeswijsheden',
    version='0.1',
    packages=[''],
    url='https://gitlab.com/pet-projects20/Tegeltjeswijsheden',
    license='MIT',
    author='Matthijs Amesz',
    author_email='matthijsamesz@hotmail.com',
    description=''
)
